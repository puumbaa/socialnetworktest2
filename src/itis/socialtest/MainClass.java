package itis.socialtest;


import itis.socialtest.entities.Author;
import itis.socialtest.entities.Post;

import java.io.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/*
 * В папке resources находятся два .csv файла.
 * Один содержит данные о постах в соцсети в следующем формате: Id автора, число лайков, дата, текст
 * Второй содержит данные о пользователях  - id, никнейм и дату рождения
 *
 * Напишите код, который превратит содержимое файлов в обьекты в package "entities"
 * и осуществите над ними следующие опреации:
 *
 * 1. Выведите в консоль все посты в читабельном виде, с информацией об авторе.
 * 2. Выведите все посты за сегодняшнюю дату
 * 3. Выведите все посты автора с ником "varlamov"
 * 4. Проверьте, содержит ли текст хотя бы одного поста слово "Россия"
 * 5. Выведите никнейм самого популярного автора (определять по сумме лайков на всех постах)
 *
 * Для выполнения заданий 2-5 используйте методы класса AnalyticsServiceImpl (которые вам нужно реализовать).
 *
 * Требования к реализации: все методы в AnalyticsService должны быть реализованы с использованием StreamApi.
 * Использование обычных циклов и дополнительных переменных приведет к снижению баллов, но допустимо.
 * Парсинг файлов и реализация методов оцениваются ОТДЕЛЬНО
 *
 *
 * */

public class MainClass {

    private List<Post> allPosts;
    private List<Author> allAuthors;

    private AnalyticsService analyticsService = new AnalyticsServiceImpl();

    public static void main(String[] args) {
        new MainClass().run(
                "C:\\Users\\user\\IdeaProjects\\socialnetworktest2\\src\\itis\\socialtest\\resources\\PostDatabase.csv",
                "C:\\Users\\user\\IdeaProjects\\socialnetworktest2\\src\\itis\\socialtest\\resources\\Authors.csv");
    }

    private void run(String postsSourcePath, String authorsSourcePath) {
        try {
            allPosts = new ArrayList<>();
            allAuthors = new ArrayList<>();
            BufferedReader brForPost = new BufferedReader(new FileReader(postsSourcePath));
            BufferedReader brForAuthor = new BufferedReader(new FileReader(authorsSourcePath));
            String s0;
            while ((s0=brForAuthor.readLine()) != null){
                String[] split = s0.split(", ");
                allAuthors.add(new Author(Long.parseLong(split[0]),split[1],split[2]));
            }

            String s;
            while ((s = brForPost.readLine()) != null) {
                String[] split = s.split(", ");
                allPosts.add(
                        new Post(split[2],split[3],Long.parseLong(split[1]),
                                analyticsService.findAuthorById(allAuthors,Long.parseLong(split[0]))));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        printPosts(allPosts);
        showTodayPosts(allPosts);
        System.out.println(analyticsService.findAllPostsByAuthorNickname(allPosts, "varlamov"));
        findPostBySearch(allPosts,"России");
        System.out.println(analyticsService.findMostPopularAuthorNickname(allPosts));
    }

    private void printPosts(List<Post> postsList) {
        for (Post post : postsList) {
            System.out.println(post);
        }
    }


    private void showTodayPosts(List<Post> posts){
        String date = new String("17.04.2021");
        System.out.println(analyticsService.findPostsByDate(posts, date));
    }
    private  void findPostBySearch(List<Post> posts, String search){
        System.out.println(posts.stream()
                .filter(post -> analyticsService.checkPostsThatContainsSearchString(posts, search))
                .findFirst().orElse(null));
    }

}
